﻿// Tutorial Script for MOGA Unity Android/WP8
// YouTube Tutorial Link: http://youtu.be/YryG2hoeLsI
// ©2013 Bensussen Deutsch and Associates, Inc. All rights reserved.

using UnityEngine;
using System.Collections;

using Input = Moga_Input;

public class RotateCube : MonoBehaviour {

	private KeyCode aButtonKeyCode		= KeyCode.Space;
	private string	horizontal			= "Horizontal",
					vertical			= "Vertical";

	// Use this for initialization
	void Start () 
	{
		// Get the MOGA Controller Manager
		Moga_ControllerManager mogaControllerManager = GameObject.Find ("MogaControllerManager").GetComponent<Moga_ControllerManager>();

		// if exists
		if (mogaControllerManager != null)
		{
			Input.RegisterMogaController();

			aButtonKeyCode 	= mogaControllerManager.p1ButtonA;
			horizontal		= mogaControllerManager.p1AxisHorizontal;
			vertical		= mogaControllerManager.p1AxisVertical;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.transform.Rotate( 0, -Input.GetAxis(horizontal), 0, Space.World );
        this.transform.Rotate( Input.GetAxis (vertical), 0, 0, Space.World );
		
		if (Input.GetKeyDown(aButtonKeyCode))
		{
			this.renderer.material.color = Color.red;
		}
		
		if (Input.GetKeyUp(aButtonKeyCode))
		{
			this.renderer.material.color = Color.white;
		}
	}
}
